const _ = require('lodash');
const animatePerSlide = require('animate-per-slide');

module.exports = {
    slide: function(args){
        if(!_.isObject(args)){
            throw('Some arguments are expected');
        }

        if(typeof args.$slide === 'undefined'){
            throw('A target slide is expected');
        }

        $('.navigation-buttons > *').removeClass('highlight');

        if(args.slide !== 'prev' && args.slide !== 'next'){
            return true;
        }

        var $groups = args.$slide.find('.group');
        if(!$groups.length){
            return  true;
        }
        else{
            var $visible = $groups.filter('.visible');
            if(!$visible.length){
                $visible = $groups.eq(0).addClass('visible');
            }
            var index = $visible.index();
            var $nextElement = args.slide === 'prev' ? $visible.prev() : $visible.next();
            if(!$nextElement.hasClass('group') || !$nextElement.length){
                return true;
            }
            if($nextElement.data('enabled') === false){
                return false;
            }
            $visible.removeClass('visible');
            $nextElement.addClass('visible');
            animatePerSlide.setAnimationClasses({
                $slide: args.$slide,
                action: 'remove'
            });
            animatePerSlide.setAnimationClasses({
                $slide: args.$slide,
                action: 'add'
            });
        }
    }
}