const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const slash = require('slash')
const fsExtra = require('fs-extra')

module.exports = {
    fs: fs,
    path: path,
    getFilesRecursively: function(dir, filelist){
        var self = this;
        const files = fs.readdirSync(dir);
        filelist = filelist || [];
        files.forEach(function(file) {
            if (fs.statSync(path.join(dir, file)).isDirectory()) {
                filelist = self.getFilesRecursively(path.join(dir, file), filelist);
            } else {
                filelist.push(path.join(dir, file));
            }
        });
        return filelist;
    },
    writeImageSources: function(){
        var self = this;
        var imageExtensions = ['.tif', '.tiff', '.ico', '.png', '.jpg', '.jpeg']
        var imageSources = self.getFilesRecursively('app/img');
        var filteredSources = [];
        _.forEach(imageSources, function(v, k){
            if(imageExtensions.includes(path.extname(v).toLowerCase())){
                var route = slash(v);
                route = route.replace('app/', '');
                filteredSources.push(route);
            }
        });
        var imageSourcesString = JSON.stringify({
            paths: filteredSources
        }, null, 4); 
        fs.writeFileSync(__dirname + '/app/imageSources.json', imageSourcesString);
        console.log('Wrote image sources in imageSources.json');

    },
    writeFontSources: function(){
        var self = this;
        var fontExtensions = ['.eot', '.otf', '.ttf', '.woff', '.svg', '.woff2']
        var fontSources = self.getFilesRecursively('app/fonts');
        var filteredSources = [];
        _.forEach(fontSources, function(v, k){
          if(fontExtensions.includes(path.extname(v).toLowerCase())){
            var route = slash(v);
            route = route.replace('app/', '');
            filteredSources.push(route);
          }
        });
        var fontSourcesString = JSON.stringify({
          paths: filteredSources
        }, null, 4); 
        fs.writeFileSync(__dirname + '/app/fontSources.json', fontSourcesString);
        console.log('Wrote font sources in fontSources.json');
    }
}